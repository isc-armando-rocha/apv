<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
        'user_name'=>'arm',
        'email'=>'isc.armando.rocha@gmail.com',
        'password'=> Hash::make('secret'),
        'user_paterno'=>'Rocha',
        'user_materno'=>'Mendoza',
        'user_nombre'=>'Jorge Armando',
        'user_VENTAS'=>1,
        'user_ADMINISTRAR'=>1,
        'user_REPORTES'=>1,
        'user_CATALOGOS'=>1,
        'user_CONSULTAS'=>1,
        'user_DESHACER_VENTA'=>1]);

        $user->save();

        $user->roles()->attach(Role::where('name', 'admin')->first());
    }
}

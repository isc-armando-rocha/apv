<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('user_paterno')->comment('apellido paterno');
            $table->string('user_materno')->comment('apellido materno');
            $table->string('user_nombre')->comment('nombre');
            $table->boolean('user_VENTAS')->comment('Permiso de Ventas');
            $table->boolean('user_ADMINISTRAR')->comment('Permiso de Administrar');
            $table->boolean('user_REPORTES')->comment('Permiso de Reportes');
            $table->boolean('user_CATALOGOS')->comment('Permiso de Catalogos');
            $table->boolean('user_CONSULTAS')->comment('Permiso de Consultas');
            $table->boolean('user_DESHACER_VENTA')->comment('Permiso de Deshacer Ventas');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

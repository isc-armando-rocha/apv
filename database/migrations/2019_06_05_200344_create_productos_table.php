<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('dep_id')->nullable();
            $table->integer('medida_id');
            $table->string('desc_producto')->nullable();
            $table->double('prod_preCost',5,2)->nullable();
            $table->double('prod_preVent',5,2);
            $table->double('prod_preMay',5,2)->nullable();
            $table->double('prod_existencia',5,2)->nullable();
            $table->double('prod_invMin',5,2)->nullable();
            $table->double('prod_iva',5,2)->nullable();
            $table->string('prod_sMay');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}

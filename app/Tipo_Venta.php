<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_Venta extends Model
{
    protected $fillable = [
        'desc_tipo_venta',
    ];
}

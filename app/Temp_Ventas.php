<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temp_Ventas extends Model
{
    protected $fillable = [
        'id_prod',
        'id_user',
        'cantidad',
        'p_unitario',
        'iva'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $fillable = [
        'paterno',
        'materno',
        'nombre',
        'direccion',
        'telefono',
        'credito',
        'lim_credito'

    ];
}

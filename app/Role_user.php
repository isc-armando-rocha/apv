<?php
/**
 * Created by PhpStorm.
 * User: osito
 * Date: 10/07/2019
 * Time: 05:28 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Role_user extends Model

{
    use Notifiable;
    protected $table = 'role_user';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'user_id',
        'role_id',
    ];
}

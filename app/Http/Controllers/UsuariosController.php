<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsuariosController extends Controller
{
    /**
     * Metodos User
     */
    public function user()
    {
        $sql=User::select('users.id AS user_id', 'user_name', 'email', 'user_paterno', 'user_materno', 'user_nombre', 'description')
            ->Join('role_user', 'users.id', '=', 'role_user.user_id')
            ->Join('roles', 'roles.id', '=', 'role_user.role_id' )->paginate(10);
        return view('ventas.user', ['users'=>$sql]);
    }
    public function user_create()
    {
        $adm =0;
        $user='empl';
        if (request()->category_e=='admin') {
            $adm = 1;
            $user='admin';
        }
        $userS = User::create([
            'user_name' => request()->alias,
            'email' => request()->mail,
            'password' => Hash::make('secret'),
            'user_paterno' => request()->ape,
            'user_materno' => request()->ame,
            'user_nombre' => request()->name,
            'user_VENTAS' => 1,
            'user_ADMINISTRAR' => $adm,
            'user_REPORTES' => $adm,
            'user_CATALOGOS' => 1,
            'user_CONSULTAS' => 1,
            'user_DESHACER_VENTA' => $adm
        ]);
        $userS->save();
        $userS->roles()->attach(Role::where('name', $user)->first());
        return response()->json(['success' => 'Registro agregado satisfactoriamente.', 'user'=>$userS]);
    }
    public function user_delete()
    {
        $userD = User::find(request()->d_id);
        $userD->delete();
        return response()->json(['success' => 'Registro Eliminado satisfactoriamente.']);
    }
    public function user_update()
    {
        $adm =0;
        $user='empl';
        $userE = User::find(request()->edit_id);
        if (request()->edit_category_e=="admin"){
            $adm = 1;
            $user='admin';
        }
        $userE->user_name = request()->edit_alias;
        $userE->email = request()->edit_mail;
        $userE->user_paterno = request()->edit_ape;
        $userE->user_materno = request()->edit_ame;
        $userE->user_nombre = request()->edit_name;
        $userE->user_VENTAS = 1;
        $userE->user_ADMINISTRAR = $adm;
        $userE->user_REPORTES = $adm;
        $userE->user_CATALOGOS = 1;
        $userE->user_CONSULTAS = 1;
        $userE->user_DESHACER_VENTA = $adm;
        if (request()->edit_pass!=""){
            $userE->password= Hash::make(request()->edit_pass);
        }
        $userE->save();

        $role = Role_user::where('user_id', '=', $userE->id);
        $role->delete();

        $userE->roles()->attach(Role::where('name', $user)->first());
        return response()->json(['success' => 'Registro modificado satisfactoriamente.', 'user'=>$userE]);
    }
}

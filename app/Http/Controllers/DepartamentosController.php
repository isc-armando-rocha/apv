<?php

namespace App\Http\Controllers;

use App\Departamentos;
use Illuminate\Http\Request;

class DepartamentosController extends Controller
{
    /*** Metodos Departamentos*/
    public function deps()
    {
        $sql=Departamentos::all();
        return view('ventas.dep', ['deps'=>$sql]);
    }
    public function deps_create()
    {
        $dep = Departamentos::create([
            'dep_nombre' => request()->namedep,
            'dep_descripcion' => request()->dep_desc
        ]);
        $dep->save();
        return response()->json(['success' => 'Registro agregado satisfactoriamente.', 'dep'=>$dep]);
    }
    public function deps_delete()
    {
        $userD = Departamentos::find(request()->d_id);
        $userD->delete();
        return response()->json(['success' => 'Registro Eliminado satisfactoriamente.']);
    }
    public function deps_update()
    {
        $dep = Departamentos::find(request()->edit_id);
        $dep->dep_nombre = request()->edit_namedep;
        $dep->dep_descripcion = request()->edit_dep_desc;
        $dep->save();

        return response()->json(['success' => 'Registro modificado satisfactoriamente.', 'user'=>$dep]);
    }
}

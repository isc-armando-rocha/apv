<?php

namespace App\Http\Controllers;

use App\Clientes;
use App\Departamentos;
use App\Productos;
use App\Role;
use App\Role_user;
use App\User;
use Illuminate\Support\Facades\Hash;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class PvController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('pv.dashboard');
    }

    public function dashboard()
    {
        $deps= Departamentos::all()->count();
        $caj = User::all()->count();
        $prod = Productos::all()->count();
        return view ('pv.dashboard', ['deps'=>$deps, 'caj'=> $caj, 'prod'=>$prod]);
    }











}

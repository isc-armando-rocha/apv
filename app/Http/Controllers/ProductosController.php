<?php

namespace App\Http\Controllers;

use App\Departamentos;
use App\Productos;
use Illuminate\Http\Request;

class ProductosController extends Controller
{
    /*** Metodos Productos*/
    public function prods()
    {
        $sql=Productos::select('productos.id', 'productos.dep_id', 'productos.medida_id', 'productos.prod_nombre', 'productos.prod_descripcion', 'productos.prod_preCost', 'productos.prod_preVent', 'productos.prod_preMay', 'productos.prod_bInv', 'productos.prod_existencia',  'productos.prod_invMin', 'productos.prod_iva', 'productos.prod_sMay', 'unidad_medida.uni_desc')->Join('unidad_medida', 'productos.medida_id', '=', 'unidad_medida.id')->get();

        $deps=Departamentos::all();

        return view('ventas.prods', ['prods'=>$sql, 'deps'=>$deps]);
    }
    public function prods_create()
    {
        $inv = request()->inv==true? 1:0;
        $invm = $inv==1? request()->nv_min : 0;
        $invcan = $inv==1? request()->cat_inv :0;
        $prod = Productos::create([
            'id' => request()->id,
            'dep_id' => request()->depa,
            'medida_id' => request()->umed,
            'prod_nombre' => request()->name,
            'prod_descripcion' => request()->desc,
            'prod_preCost' => (int)request()->pre_costo,
            'prod_preVent' => (int)request()->pre_vent,
            'prod_preMay' => (int)request()->pre_may,
            'prod_bInv' => $inv,
            'prod_existencia' => $invcan,
            'prod_invMin' => $invm,
            'prod_iva' => 1,
            'prod_sMay' => 1
        ]);
        $prod->save();
        return response()->json(['success' => 'Registro agregado satisfactoriamente.', 'user'=>$prod]);
    }
    public function prods_delete()
    {
        $prod = Productos::find(request()->d_id);
        $prod->delete();
        return response()->json(['success' => 'Registro Eliminado satisfactoriamente.']);
    }
    public function prods_update()
    {
        $inv = request()->edit_inv==true? 1:0;
        $invm = $inv==1? request()->edit_inv_min : 0;
        $invcan = $inv==1? request()->edit_cat_inv :0;

        $prod = Productos::find(request()->edit_id);
        $prod->id = request()->edit_id;
        $prod->dep_id = request()->edit_depa;
        $prod->medida_id = request()->edit_umed;
        $prod->prod_nombre = request()->edit_name;
        $prod->prod_descripcion = request()->edit_desc;
        $prod->prod_preCost = (int)request()->edit_pre_costo;
        $prod->prod_preVent = (int)request()->edit_pre_vent;
        $prod->prod_preMay = (int)request()->edit_pre_may;
        $prod->prod_bInv = $inv;
        $prod->prod_existencia = $invcan;
        $prod->prod_invMin = $invm;
        $prod->prod_iva = 1;
        $prod->prod_sMay = 1;
        $prod->save();

        return response()->json(['success' => 'Registro modificado satisfactoriamente.', 'Prod'=>$prod]);
    }
}

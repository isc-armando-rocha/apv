<?php

namespace App\Http\Controllers;

use App\Departamentos;
use App\Productos;
use Illuminate\Http\Request;

class InventariosController extends Controller
{
    public function inven()
    {
        $sql=Productos::select('productos.id', 'productos.dep_id', 'productos.medida_id', 'productos.prod_nombre', 'productos.prod_descripcion', 'productos.prod_preCost', 'productos.prod_preVent', 'productos.prod_preMay', 'productos.prod_bInv', 'productos.prod_existencia',  'productos.prod_invMin', 'productos.prod_iva', 'productos.prod_sMay', 'unidad_medida.uni_desc')->Join('unidad_medida', 'productos.medida_id', '=', 'unidad_medida.id')->get();

        $deps=Departamentos::all();

        return view('ventas.inven', ['inven'=>$sql, 'deps'=>$deps]);
    }
    public function inven_create()
    {
        $inv = request()->inv==true? 1:0;
        $prod = Productos::create([
            'id' => request()->id,
            'dep_id' => request()->depa,
            'medida_id' => request()->umed,
            'prod_nombre' => request()->name,
            'prod_descripcion' => request()->desc,
            'prod_preCost' => (int)request()->pre_costo,
            'prod_preVent' => (int)request()->pre_vent,
            'prod_preMay' => (int)request()->pre_may,
            'prod_bInv' => $inv,
            'prod_existencia' => request()->cat_inv,
            'prod_invMin' => request()->nv_min,
            'prod_iva' => 1,
            'prod_sMay' => 1
        ]);
        $prod->save();
        return response()->json(['success' => 'Registro agregado satisfactoriamente.', 'user'=>$prod]);
    }
    public function inven_delete()
    {

    }
    public function inven_update()
    {

    }
}

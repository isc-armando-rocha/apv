<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectPath()
    {
        if (\Auth::user()->hasrole('admin')){
            return '/admin/dashboard';
        }
        if (\Auth::user()->hasrole('clien')){
            return '/client/dashboard';
        }
        return '/login';
    }

    protected function authenticated()
    {
        if(auth()->user()->hasrole('admin')){
            return redirect()->route('admin.dash') ;
        }
        else{
            abort(401, 'Existen problemas con sus credenciales');
        }
    }
}

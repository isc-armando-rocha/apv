<?php

namespace App\Http\Controllers;

use App\Clientes;
use Illuminate\Http\Request;

class ClientesController extends Controller
{
    /*** Metodos Clientes*/
    public function client()
    {
        $sql=Clientes::all();
        return view('ventas.clients', ['clients'=>$sql]);
    }
    public function client_create()
    {
        $client = Clientes::create([
            'paterno' => request()->ape,
            'materno' => request()->ame,
            'nombre' => request()->name,
            'direccion' => request()->dir,
            'telefono' => request()->tel,
            'lim_credito' => request()->limcred,
            'credito' => 0
        ]);
        $client->save();
        return response()->json(['success' => 'Registro agregado satisfactoriamente.', 'user'=>$client]);
    }
    public function client_delete()
    {
        $userD = Clientes::find(request()->d_id);
        $userD->delete();
        return response()->json(['success' => 'Registro Eliminado satisfactoriamente.']);
    }
    public function client_update()
    {
        $client = Clientes::find(request()->edit_id);
        $client->paterno = request()->edit_ape;
        $client->materno = request()->edit_ame;
        $client->nombre = request()->edit_name;
        $client->direccion = request()->edit_dir;
        $client->telefono = request()->edit_tel;
        $client->lim_credito = request()->edit_limcred;
        $client->save();

        return response()->json(['success' => 'Registro modificado satisfactoriamente.', 'user'=>$client]);
    }
}

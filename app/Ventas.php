<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ventas extends Model
{
    protected $fillable = [
        'id',
        'fecha',
        'id_user',
        'total',
        'id_caja',
        'id_client'
    ];
}

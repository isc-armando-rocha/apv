<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamentos extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = [
        'dep_nombre',
        'dep_descripcion'
    ];

    public function Productos ()
    {
        return $this->hasMany(Productos::class, 'dep_id', 'id');
    }
}

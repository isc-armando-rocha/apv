<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalles_Ventas extends Model
{
    protected $fillable = [
        'id_producto',
        'Folio',
        'cantidad',
        'p_unitario',
        'iva'
    ];
}

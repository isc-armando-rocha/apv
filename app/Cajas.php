<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cajas extends Model
{
    protected $fillable = [
        'id',
        'id_user',
        'desc_caja',
        'disponible',
        'ticket'
    ];
}

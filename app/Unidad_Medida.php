<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidad_Medida extends Model
{
    protected $table = 'unidad_medida';
    protected $fillable = [
        'uni_desc',
    ];

    public function Productos()
    {
        return $this->hasMany(Productos::class, 'medida_id', 'id');
    }
}

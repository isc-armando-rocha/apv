<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'dep_id',
        'medida_id',
        'prod_nombre',
        'prod_descripcion',
        'prod_preCost',
        'prod_preVent',
        'prod_preMay',
        'prod_bInv',
        'prod_existencia',
        'prod_invMin',
        'prod_iva',
        'prod_sMay'
    ];

    public function departamento()
    {
        return $this->belongsTo(Departamentos::class, 'dep_id', 'id');
    }

    public function unidad()
    {
        return $this->belongsTo(Unidad_Medida::class, 'medida_id', 'id');
    }
}

@extends('layouts.pv')

@section('title', 'APV | Usuarios')
@section('style')
    <link href="plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
    <style>
        body {
            color: #566787;
            background: #f5f5f5;
            font-family: 'Varela Round', sans-serif;
            font-size: 13px;
        }

        .table-wrapper {
            background: #fff;
            padding: 20px 25px;
            margin: 30px 0;
            border-radius: 3px;
            box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
        }

        .table-title {
            padding-bottom: 15px;
            background: #435d7d;
            color: #fff;
            padding: 16px 30px;
            margin: -20px -25px 10px;
            border-radius: 3px 3px 0 0;
        }

        .table-title h2 {
            margin: 5px 0 0;
            font-size: 24px;
        }

        .table-title .btn-group {
            float: right;
        }

        .table-title .btn {
            color: #fff;
            float: right;
            font-size: 13px;
            border: none;
            min-width: 50px;
            border-radius: 2px;
            border: none;
            outline: none !important;
            margin-left: 10px;
        }

        .table-title .btn i {
            float: left;
            font-size: 21px;
            margin-right: 5px;
        }

        .table-title .btn span {
            float: left;
            margin-top: 2px;
        }

        table.table tr th, table.table tr td {
            border-color: #e9e9e9;
            padding: 12px 15px;
            vertical-align: middle;
        }

        table.table tr th:first-child {
            width: 60px;
        }

        table.table tr th:last-child {
            width: 100px;
        }

        table.table-striped tbody tr:nth-of-type(odd) {
            background-color: #fcfcfc;
        }

        table.table-striped.table-hover tbody tr:hover {
            background: #f5f5f5;
        }

        table.table th i {
            font-size: 13px;
            margin: 0 5px;
            cursor: pointer;
        }

        table.table td:last-child i {
            opacity: 0.9;
            font-size: 22px;
            margin: 0 5px;
        }

        table.table td a {
            font-weight: bold;
            color: #566787;
            display: inline-block;
            text-decoration: none;
            outline: none !important;
        }

        table.table td a:hover {
            color: #2196F3;
        }

        table.table td a.edit {
            color: #FFC107;
        }

        table.table td a.delete {
            color: #F44336;
        }

        table.table td i {
            font-size: 19px;
        }

        table.table .avatar {
            border-radius: 50%;
            vertical-align: middle;
            margin-right: 10px;
        }

        .pagination {
            float: right;
            margin: 0 0 5px;
        }

        .pagination li a {
            border: none;
            font-size: 13px;
            min-width: 30px;
            min-height: 30px;
            color: #999;
            margin: 0 2px;
            line-height: 30px;
            border-radius: 2px !important;
            text-align: center;
            padding: 0 6px;
        }

        .pagination li a:hover {
            color: #666;
        }

        .pagination li.active a, .pagination li.active a.page-link {
            background: #03A9F4;
        }

        .pagination li.active a:hover {
            background: #0397d6;
        }

        .pagination li.disabled i {
            color: #ccc;
        }

        .pagination li i {
            font-size: 16px;
            padding-top: 6px
        }

        .hint-text {
            float: left;
            margin-top: 10px;
            font-size: 13px;
        }

        /* Custom checkbox */
        .custom-checkbox {
            position: relative;
        }

        .custom-checkbox input[type="checkbox"] {
            opacity: 0;
            position: absolute;
            margin: 5px 0 0 3px;
            z-index: 9;
        }

        .custom-checkbox label:before {
            width: 18px;
            height: 18px;
        }

        .custom-checkbox label:before {
            content: '';
            margin-right: 10px;
            display: inline-block;
            vertical-align: text-top;
            background: white;
            border: 1px solid #bbb;
            border-radius: 2px;
            box-sizing: border-box;
            z-index: 2;
        }

        .custom-checkbox input[type="checkbox"]:checked + label:after {
            content: '';
            position: absolute;
            left: 6px;
            top: 3px;
            width: 6px;
            height: 11px;
            border: solid #000;
            border-width: 0 3px 3px 0;
            transform: inherit;
            z-index: 3;
            transform: rotateZ(45deg);
        }

        .custom-checkbox input[type="checkbox"]:checked + label:before {
            border-color: #03A9F4;
            background: #03A9F4;
        }

        .custom-checkbox input[type="checkbox"]:checked + label:after {
            border-color: #fff;
        }

        .custom-checkbox input[type="checkbox"]:disabled + label:before {
            color: #b8b8b8;
            cursor: auto;
            box-shadow: none;
            background: #ddd;
        }

        /* Modal styles */
        /*.modal .modal-dialog {
            max-width: 400px;
        }*/

        .modal .modal-header, .modal .modal-body, .modal .modal-footer {
            padding: 20px 30px;
        }

        .modal .modal-content {
            border-radius: 3px;
        }

        .modal .modal-footer {
            background: #ecf0f1;
            border-radius: 0 0 3px 3px;
        }

        .modal .modal-title {
            display: inline-block;
        }

        .modal .form-control {
            border-radius: 2px;
            box-shadow: none;
            border-color: #dddddd;
        }

        .modal textarea.form-control {
            resize: vertical;
        }

        .modal .btn {
            border-radius: 2px;
            min-width: 100px;
        }

        .modal form label {
            font-weight: normal;
        }
    </style>
@endsection

@section('menu', '')

@section('title-header', 'Administracion de usuarios')

@section('content')
    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-6">
                    <h2>Administrar <b>Usuarios</b></h2>
                </div>
                <div class="col-sm-6">
                    <a href="#addProductModal" class="btn btn-success" data-toggle="modal"><i
                            class="material-icons">&#xE147;</i>
                        <span>Agregar nuevo usuario</span></a>
                </div>
            </div>
        </div>

        <div class='clearfix'></div>
        <hr>
        <div id="loader"></div><!-- Carga de datos ajax aqui -->
        <div id="resultados"></div><!-- Carga de datos ajax aqui -->
        <div class='outer_div'>
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>Usuario</th>
                    <th>Categoría</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr class="">
                        <td class="text-center">{{$user->user_id}}</td>
                        <td>{{$user->user_name}} {{$user->user_paterno}} {{$user->user_materno}}</td>
                        <td>{{$user->description}}</td>
                        <td>
                            <a href="#" data-target="#editProductModal" class="edit" data-toggle="modal"
                               data-name="{{$user->user_name}}" data-category="{{$user->description}}"
                               data-paterno="{{$user->user_paterno}}"
                               data-materno="{{$user->user_materno}}"
                               data-id="{{$user->user_id}}"><i class="material-icons" data-toggle="tooltip"
                                                               title="Editar"></i></a>
                            <a href="#deleteProductModal" class="delete" data-toggle="modal"
                               data-id="{{$user->user_id}}"><i
                                    class="material-icons" data-toggle="tooltip" title="Eliminar"></i></a>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="6">
                        {{ $users->links() }}
                    </td>
                </tr>
                </tbody>
            </table>
        </div><!-- Carga de datos ajax aqui -->


    </div>

@endsection

@section('script')
    <script>


        function load(page) {
            var query = $("#q").val();
            var per_page = 10;
            var parametros = {"action": "ajax", "page": page, 'query': query, 'per_page': per_page};
            $("#loader").fadeIn('slow');
            $.ajax({
                url: 'usuarios_list',
                data: parametros,
                beforeSend: function (objeto) {
                    $("#loader").html("Cargando...");
                },
                success: function (data) {
                    $(".outer_div").html(data).fadeIn('slow');
                    $("#loader").html("");
                }
            })
        }

        $('#editProductModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var code = button.data('code')
            $('#edit_code').val(code)
            var name = button.data('name')
            $('#edit_name').val(name)
            var category = button.data('category')
            $('#edit_category').val(category)
            var stock = button.data('stock')
            $('#edit_stock').val(stock)
            var price = button.data('price')
            $('#edit_price').val(price)
            var id = button.data('id')
            $('#edit_id').val(id)
        })

        $('#deleteProductModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id')
            $('#delete_id').val(id)
        })


        $("#edit_product").submit(function (event) {
            var parametros = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "ajax/editar_producto.php",
                data: parametros,
                beforeSend: function (objeto) {
                    $("#resultados").html("Enviando...");
                },
                success: function (datos) {
                    $("#resultados").html(datos);
                    load(1);
                    $('#editProductModal').modal('hide');
                }
            });
            event.preventDefault();
        });


        $("#add_product").submit(function (event) {
            var parametros = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "usuarios_create",
                data: parametros,
                beforeSend: function (objeto) {
                    $("#resultados").html("Enviando...");
                },
                success: function (datos) {
                    if(confirm(datos.responseText)){
                        window.location.reload();
                    }
                    console.log(datos);
                    /*$("#resultados").html(datos.user);*/
                    /*load(1);*/

                    $('#addProductModal').modal('hide');
                },
                error: function (datos) {
                    console.log(datos.responseText);
                }
            });
            event.preventDefault();
        });

        $("#delete_product").submit(function (event) {
            var parametros = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "ajax/eliminar_producto.php",
                data: parametros,
                beforeSend: function (objeto) {
                    $("#resultados").html("Enviando...");
                },
                success: function (datos) {
                    $("#resultados").html(datos);
                    load(1);
                    $('#deleteProductModal').modal('hide');
                }
            });
            event.preventDefault();
        });

    </script>
@endsection

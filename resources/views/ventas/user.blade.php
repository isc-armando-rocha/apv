@extends('layouts.pv')

@section('title', 'APV | Usuarios')
@section('style')
    <link href="plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
    <link href="plugins/sweetalert/sweetalert.css" rel="stylesheet" />
    <!-- JQuery DataTable Css -->
    <link href="plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

@endsection

@section('menu')
<li>
    <a href="dashboard">
        <i class="material-icons">home</i>
        <span>Inicio</span>
    </a>
</li>
<li>
    <a href="clientes">
        <i class="material-icons">assignment_ind</i>
        <span>Clientes</span>
    </a>
</li>
<li>
    <a href="productos">
        <i class="material-icons">shopping_cart</i>
        <span>Productos</span>
    </a>
</li>
<li>
    <a href="ventas">
        <i class="material-icons">attach_money</i>
        <span>Ventas</span>
    </a>
</li>
<li>
    <a href="departamentos">
        <i class="material-icons">list</i>
        <span>Departamentos</span>
    </a>
</li>
<li>
    <a href="rventas">
        <i class="material-icons">update</i>
        <span>Reporte de ventas</span>
    </a>
</li>
<li>
    <a href="inventario">
        <i class="material-icons">view_module</i>
        <span>Inventario</span>
    </a>
</li>
<li class="active">
    <a href="usuarios">
        <i class="material-icons">face</i>
        <span>Cajeros</span>
    </a>
</li>
@endsection
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Administrar <b>Usuarios</b>
                        <a href="#addUserModal" class="btn btn-success pull-right" data-toggle="modal"><i
                                class="material-icons">&#xE147;</i>
                            <span>Agregar nuevo usuario</span></a>
                    </h2>

                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Usuario</th>
                                <th class="text-center">Alias</th>
                                <th class="text-center">Categoría</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Usuario</th>
                                <th class="text-center">Alias</th>
                                <th class="text-center">Categoría</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                            </tfoot>
                            <tbody>
                                @php $i = 1; @endphp
                                @foreach($users as $user)
                                    <tr class="">
                                        <td class="text-center">{{$i}}</td>
                                        <td class="text-center">{{$user->user_nombre}} {{$user->user_paterno}} {{$user->user_materno}}</td>
                                        <td class="text-center">{{$user->user_name}}</td>
                                        <td class="text-center">{{$user->description}}</td>
                                        <td class="text-center">
                                            <a href="#" data-target="#editUserModal" class="edit" data-toggle="modal"
                                               data-name="{{$user->user_name}}" data-category="{{$user->description}}"
                                               data-paterno="{{$user->user_paterno}}"
                                               data-materno="{{$user->user_materno}}"
                                               data-id="{{$user->user_id}}"
                                               data-mail="{{$user->email}}"
                                               data-nombre="{{$user->user_nombre}}"><i class="material-icons" data-toggle="tooltip"
                                                                               title="Editar"></i></a>
                                            <a href="#deleteUserModal" class="delete" data-toggle="modal"
                                               data-id="{{$user->user_id}}" data-name="{{$user->user_name}}"><i
                                                    class="material-icons" data-toggle="tooltip" title="Eliminar"></i></a>
                                        </td>
                                    </tr>
                                    @php $i ++; @endphp
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="addUserModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-col-blue">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Agregar Usuario</h4>
                </div>
                <form name="add_product" id="add_user" method="POST" action="{{ route('admin.user_create') }}">
                    <div class="modal-body" style="background-color: #ffffff;color: #000 !important;">
                        @csrf
                        <div class="row">
                                <div class="col-md-6">
                                    <label for="name">Nombre</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="name" id="name" class="form-control" placeholder="Ingresa el nombre del cajero">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="ape">Apellido Paterno</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="ape" id="ape" class="form-control" placeholder="Apellido paterno">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="ame">Apellido Materno</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="ame" id="ame" class="form-control" placeholder="Apellido materno">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="mail">Correo</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="email" name="mail" id="mail" class="form-control" placeholder="Correo">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="pass">Contraseña</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" name="pass" id="pass" class="form-control" placeholder="Contraseña">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="alias">Alias</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="alias" id="alias" class="form-control" placeholder="Alias">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="category_e">Tipo</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select id="category_e" name="category_e" class="form-control show-tick">
                                            <option value="empl">Cajero</option>
                                            <option value="admin">Administrador</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">GUARDAR</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="editUserModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-col-green">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Editar Usuario</h4>
                </div>
                <form name="add_product" id="edit_user" method="POST" action="{{ route('admin.user_update') }}">
                    <div class="modal-body" style="background-color: #ffffff;color: #000 !important;">
                        @csrf
                        <div class="row">
                            <input type="hidden" name="edit_id" id="edit_id"  value="">
                            <div class="col-md-6">
                                <label for="edit_name">Nombre</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="edit_name" id="edit_name" class="form-control" placeholder="Ingresa el nombre del cajero">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="edit_ape">Apellido Paterno</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="edit_ape" id="edit_ape" class="form-control" placeholder="Apellido paterno">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="edit_ame">Apellido Materno</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="edit_ame" id="edit_ame" class="form-control" placeholder="Apellido materno">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="edit_mail">Correo</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="email" name="edit_mail" id="edit_mail" class="form-control" placeholder="Correo">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="edit_pass">Contraseña</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" name="edit_pass" id="edit_pass" class="form-control" placeholder="Contraseña">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="edit_alias">Alias</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="edit_alias" id="edit_alias" class="form-control" placeholder="Alias">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="edit_category_e">Tipo</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select id="edit_category_e" name="edit_category_e" class="form-control show-tick">
                                            <option value="cajer">Cajero</option>
                                            <option value="admin">Administrador</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">GUARDAR</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="deleteUserModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-col-red">
                <form name="delete_user" id="delete_user" method="POST" action="{{ route('admin.user_delete') }}">
                    @csrf
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Eliminar Usuario</h4>
                    </div>
                        <div class="modal-body" style="background-color: #ffffff;color: #000 !important;">
                            <p>¿Seguro que quieres eliminar este registro?</p>
                            <p class="text-warning">
                                <small>Esta acción no se puede deshacer.</small>
                            </p>
                            <input type="hidden" name="d_id" id="d_id" value="">
                        </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">ELIMINAR</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <script src="plugins/sweetalert/sweetalert-dev.js"></script>

    <!-- Custom Js -->
    <script src="js/pages/tables/jquery-datatable.js"></script>
    <script>

        $('#editUserModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var id = button.data('id');
            $('#edit_id').val(id);
            var name = button.data('name');
            $('#edit_alias').val(name);
            var paterno = button.data('paterno');
            $('#edit_ape').val(paterno);
            var materno = button.data('materno');
            $('#edit_ame').val(materno);
            var nombre = button.data('nombre');
            $('#edit_name').val(nombre);
            var mail = button.data('mail');
            $('#edit_mail').val(mail);
            var description = button.data('category');

            var select=document.getElementById("edit_category_e");
            for(var i=1;i<select.length;i++){
                if(select.options[i].text==description)
                {
                    // seleccionamos el valor que coincide
                    select.selectedIndex=i;
                }
            }
        });

        $('#deleteUserModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var id = button.data('id');
            $('#d_id').val(id);
        });


        $("#edit_user").submit(function (event) {
            var parametros = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('admin.user_update') }}",
                data: parametros,
                beforeSend: function (objeto) {
                    $("#resultados").html("Enviando...");
                },
                success: function (datos) {
                    swal({
                        title: "Muy bien!",
                        text: datos.success,
                        icon: "success",
                        timer: 3000,
                        showConfirmButton: false,
                    });
                    setTimeout(function(){
                        window.location.reload();
                        $('#editUserModal').modal('hide');
                    },3000);
                }
            });
            event.preventDefault();
        });


        $("#add_user").submit(function (event) {
            var parametros = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "usuarios_create",
                data: parametros,
                beforeSend: function (objeto) {
                    $("#resultados").html("Enviando...");
                },
                success: function (datos) {
                    swal({
                        title: "Muy bien!",
                        text: datos.success,
                        icon: "success",
                        timer: 5000,
                        showConfirmButton: false,
                    });
                    setTimeout(function(){
                        window.location.reload();
                        self.close();
                    },3000);
                },
                error: function (datos) {
                    console.log(datos.responseText);
                }
            });
            event.preventDefault();
        });

        $("#delete_user").submit(function (event) {
            var parametros = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "usuarios_delete",
                data: parametros,
                beforeSend: function (objeto) {
                    $("#resultados").html("Enviando...");
                },
                success: function (datos) {
                    swal({
                        title: "Muy bien!",
                        text: datos.success,
                        icon: "success",
                        timer: 5000,
                        showConfirmButton: false,
                    });
                    setTimeout(function(){
                        window.location.reload();
                        self.close();
                    },3000);window.location.reload();
                }
            });
            event.preventDefault();
        });

    </script>
@endsection

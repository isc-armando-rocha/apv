@extends('layouts.pv')

@section('title', 'APV | Usuarios')
@section('style')
    <link href="plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
    <link href="plugins/sweetalert/sweetalert.css" rel="stylesheet" />
    <!-- JQuery DataTable Css -->
    <link href="plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

@endsection

@section('menu')
<li>
    <a href="dashboard">
        <i class="material-icons">home</i>
        <span>Inicio</span>
    </a>
</li>
<li>
    <a href="clientes">
        <i class="material-icons">assignment_ind</i>
        <span>Clientes</span>
    </a>
</li>
<li>
    <a href="productos">
        <i class="material-icons">shopping_cart</i>
        <span>Productos</span>
    </a>
</li>
<li>
    <a href="ventas">
        <i class="material-icons">attach_money</i>
        <span>Ventas</span>
    </a>
</li>
<li class="active">
    <a href="departamentos">
        <i class="material-icons">list</i>
        <span>Departamentos</span>
    </a>
</li>
<li>
    <a href="rventas">
        <i class="material-icons">update</i>
        <span>Reporte de ventas</span>
    </a>
</li>
<li>
    <a href="inventario">
        <i class="material-icons">view_module</i>
        <span>Inventario</span>
    </a>
</li>
<li>
    <a href="usuarios">
        <i class="material-icons">face</i>
        <span>Cajeros</span>
    </a>
</li>
@endsection
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Administrar <b>Departamentos</b>
                        <a href="#addDepModal" class="btn btn-success pull-right" data-toggle="modal"><i
                                class="material-icons">&#xE147;</i>
                            <span>Agregar nuevo departamento</span></a>
                    </h2>

                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Nombre</th>
                                <th class="text-center">Descripción</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Nombre</th>
                                <th class="text-center">Descripción</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                            </tfoot>
                            <tbody>
                                @php $i = 1; @endphp
                                @foreach($deps as $dep)
                                    <tr class="">
                                        <td class="text-center">{{$i}}</td>
                                        <td class="text-center">{{$dep->dep_nombre}}</td>
                                        <td class="text-center">{{$dep->dep_descripcion}}</td>
                                        <td class="text-center">
                                            <a href="#" data-target="#editDepModal" class="edit" data-toggle="modal"
                                               data-id="{{$dep->id}}"
                                               data-dep_nombre="{{$dep->dep_nombre}}"
                                               data-dep_desc="{{$dep->dep_descripcion}}">
                                                <i class="material-icons" data-toggle="tooltip"
                                                                               title="Editar"></i></a>
                                            <a href="#deleteDepModal" class="delete" data-toggle="modal"
                                               data-id="{{$dep->id}}" data-name="{{$dep->user_name}}"><i
                                                    class="material-icons" data-toggle="tooltip" title="Eliminar"></i></a>
                                        </td>
                                    </tr>
                                    @php $i ++; @endphp
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="addDepModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content modal-col-blue">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Agregar Departamento</h4>
                </div>
                <form id="add_dep" method="POST" action="{{ route('admin.dep_create') }}">
                    <div class="modal-body" style="background-color: #ffffff;color: #000 !important;">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <label for="namedep">Nombre</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="namedep" id="namedep" class="form-control" placeholder="Ingresa el nombre del departamento">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="dep_desc">Descripción</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="dep_desc" id="dep_desc" class="form-control" placeholder="Opcional">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">GUARDAR</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="editDepModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content modal-col-green">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Editar Departamento</h4>
                </div>
                <form id="edit_dep" method="POST" action="{{ route('admin.dep_update') }}">
                    <div class="modal-body" style="background-color: #ffffff;color: #000 !important;">
                        @csrf
                        <div class="row">
                            <input type="hidden" name="edit_id" id="edit_id"  value="">
                            <div class="col-md-6 col-md-offset-3">
                                <label for="edit_namedep">Nombre</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="edit_namedep" id="edit_namedep" class="form-control" placeholder="Ingresa el nombre del departamento">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="edit_dep_desc">Descripción</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="edit_dep_desc" id="edit_dep_desc" class="form-control" placeholder="Opcional">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">GUARDAR</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="deleteDepModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-col-red">
                <form id="delete_dep" method="POST" action="{{ route('admin.dep_delete') }}">
                    @csrf
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Eliminar Usuario</h4>
                    </div>
                        <div class="modal-body" style="background-color: #ffffff;color: #000 !important;">
                            <p>¿Seguro que quieres eliminar este registro?</p>
                            <p class="text-warning">
                                <small>Esta acción no se puede deshacer.</small>
                            </p>
                            <input type="hidden" name="d_id" id="d_id" value="">
                        </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">ELIMINAR</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <script src="plugins/sweetalert/sweetalert-dev.js"></script>

    <!-- Custom Js -->
    <script src="js/pages/tables/jquery-datatable.js"></script>
    <script>

        $('#editDepModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var id = button.data('id');
            $('#edit_id').val(id);
            var nombre = button.data('dep_nombre');
            $('#edit_namedep').val(nombre);
            var desc = button.data('dep_desc');
            $('#edit_dep_desc').val(desc);
        });

        $('#deleteDepModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var id = button.data('id');
            $('#d_id').val(id);
        });

        $("#edit_dep").submit(function (event) {
            var parametros = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('admin.dep_update') }}",
                data: parametros,
                beforeSend: function (objeto) {
                    $("#resultados").html("Enviando...");
                },
                success: function (datos) {
                    swal({
                        title: "Muy bien!",
                        text: datos.success,
                        icon: "success",
                        timer: 3000,
                        showConfirmButton: false,
                    });
                    setTimeout(function(){
                        window.location.reload();
                        $('#editDeptModal').modal('hide');
                    },3000);
                }
            });
            event.preventDefault();
        });

        $("#add_dep").submit(function (event) {
            var parametros = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('admin.dep_create') }}",
                data: parametros,
                beforeSend: function (objeto) {
                    $("#resultados").html("Enviando...");
                },
                success: function (datos) {
                    swal({
                        title: "Muy bien!",
                        text: datos.success,
                        icon: "success",
                        timer: 3000,
                        showConfirmButton: false,
                    });
                    setTimeout(function(){
                        window.location.reload();
                        $('#addDepModal').modal('hide');
                    },3000);
                },
                error: function (datos) {
                    console.log(datos.responseText);
                }
            });
            event.preventDefault();
        });

        $("#delete_dep").submit(function (event) {
            var parametros = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('admin.dep_delete') }}",
                data: parametros,
                beforeSend: function (objeto) {
                    $("#resultados").html("Enviando...");
                },
                success: function (datos) {
                    swal({
                        title: "Muy bien!",
                        text: datos.success,
                        icon: "success",
                        timer: 3000,
                        showConfirmButton: false,
                    });
                    setTimeout(function(){
                        window.location.reload();
                        $('#deleteDepModal').modal('hide');
                    },3000);
                }
            });
            event.preventDefault();
        });

    </script>
@endsection

@php setlocale(LC_MONETARY, 'es_MX')@endphp
@extends('layouts.pv')

@section('title', 'APV | Usuarios')
@section('style')
    <link href="plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
    <link href="plugins/sweetalert/sweetalert.css" rel="stylesheet" />
    <!-- JQuery DataTable Css -->
    <link href="plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet">

@endsection

@section('menu')
    <li>
        <a href="dashboard">
            <i class="material-icons">home</i>
            <span>Inicio</span>
        </a>
    </li>
    <li>
        <a href="clientes">
            <i class="material-icons">assignment_ind</i>
            <span>Clientes</span>
        </a>
    </li>
    <li>
        <a href="productos">
            <i class="material-icons">shopping_cart</i>
            <span>Productos</span>
        </a>
    </li>
    <li>
        <a href="ventas">
            <i class="material-icons">attach_money</i>
            <span>Ventas</span>
        </a>
    </li>
    <li>
        <a href="departamentos">
            <i class="material-icons">list</i>
            <span>Departamentos</span>
        </a>
    </li>
    <li>
        <a href="rventas">
            <i class="material-icons">update</i>
            <span>Reporte de ventas</span>
        </a>
    </li>
    <li  class="active">
        <a href="inventario">
            <i class="material-icons">view_module</i>
            <span>Inventario</span>
        </a>
    </li>
    <li>
        <a href="usuarios">
            <i class="material-icons">face</i>
            <span>Cajeros</span>
        </a>
    </li>
@endsection
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Administrar <b>Productos</b>
                        <a href="#addProdModal" class="btn btn-success pull-right" data-toggle="modal"><i class="material-icons">&#xE147;</i>
                            <span>Agregar nuevo producto</span></a>
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable table-responsive text-nowrap">
                            <thead class="thead-dark">
                            <tr>
                                <th class="text-center" scope="col">#</th>
                                <th class="text-center" scope="col">Nombre</th>
                                <th class="text-center" scope="col">Descripcion</th>
                                <th class="text-center" scope="col">Precio Costo</th>
                                <th class="text-center" scope="col">Precio Venta</th>
                                <th class="text-center" scope="col">Departamento</th>
                                <th class="text-center" scope="col">Se Vende</th>
                                <th class="text-center" scope="col">Existencia</th>
                                <th class="text-center" scope="col">Inventario Minimo</th>
                            </tr>
                            </thead>
                            <tfoot class="thead-dark">
                            <tr>
                                <th class="text-center" scope="col">#</th>
                                <th class="text-center" scope="col">Nombre</th>
                                <th class="text-center" scope="col">Descripcion</th>
                                <th class="text-center" scope="col">Precio Costo</th>
                                <th class="text-center" scope="col">Precio Venta</th>
                                <th class="text-center" scope="col">Departamento</th>
                                <th class="text-center" scope="col">Se Vende</th>
                                <th class="text-center" scope="col">Existencia</th>
                                <th class="text-center" scope="col">Inventario Minimo</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @php $i = 1; @endphp
                            @foreach($inven as $prod)
                                <tr class="">
                                    <th scope="row" class="text-center">{{$prod->id}}</th>
                                    <th class="text-center">{{$prod->prod_nombre}}</th>
                                    <th class="text-center">{{$prod->prod_descripcion}}</th>
                                    <th class="text-center">{{money_format('%.2n',$prod->prod_preCost)}}</th>
                                    <th class="text-center">{{money_format('%.2n',$prod->prod_preVent)}}</th>
                                    <th class="text-center">{{$prod->departamento()->first()->dep_nombre}}</th>
                                    <th class="text-center">{{$prod->uni_desc}}</th>
                                    <th class="text-center {{($prod->prod_existencia < $prod->prod_invMin ? 'bg-red' : '') }}">{{$prod->prod_existencia}}</th>
                                    <th class="text-center">{{($prod->prod_bInv==1? $prod->prod_invMin : '')}}</th>
                                    <!--td class="text-center">
                                        <a href="#" data-target="#editProdModal" class="edit"
                                           data-toggle="modal"
                                           data-id="{{$prod->id}}"
                                           data-nombre="{{$prod->prod_nombre}}"
                                           data-desc="{{$prod->prod_descripcion}}"
                                           data-cost="{{$prod->prod_preCost}}"
                                           data-vent="{{$prod->prod_preVent}}"
                                           data-premay="{{$prod->prod_preMay}}"
                                           data-medida="{{$prod->medida_id}}"
                                           data-binv="{{$prod->prod_bInv}}"
                                           data-invmin="{{$prod->prod_invMin}}"
                                           data-exist="{{$prod->prod_existencia}}"
                                           data-depa="{{$prod->dep_id}}"><i class="material-icons"
                                                                            data-toggle="tooltip" title="Editar"></i></a>
                                        <a href="#deleteProdModal" class="delete" data-toggle="modal"
                                           data-id="{{$prod->id}}"
                                           data-name="{{$prod->prod_nombre}}"><i class="material-icons" data-toggle="tooltip" title="Eliminar"></i></a>
                                    </td-->
                                </tr>
                                @php $i ++; @endphp
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="addProdModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-col-blue">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Agregar Usuario</h4>
                </div>
                <form  id="add_product" >
                    <div class="modal-body" style="background-color: #ffffff;color: #000 !important;">
                        @csrf
                        <div class="row">
                            <div class="col-md-3">
                                <label for="id">Codigo</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="id" id="id" class="form-control" placeholder="Ingresa el codigo del producto">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="name">Nombre</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Nombre">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="desc">Descripción</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input name="desc"  id="desc" class="form-control" placeholder="Desripción del producto"></input>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-7">
                                <label for=""> Se Vende</label>
                                <div class="demo-radio-button">
                                    <input name="umed" type="radio" id="radio_7" class="radio-col-red" checked value="1" />
                                    <label for="radio_7">Por Unidad/Pza</label>
                                    <input name="umed" type="radio" id="radio_8" class="radio-col-pink" value="2" />
                                    <label for="radio_8">A Granel <span>(usa decimales)</span></label>
                                    <input name="umed" type="radio" id="radio_9" class="radio-col-purple" value="3" />
                                    <label for="radio_9">Como Paquete <span>(kit)</span></label>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label for="departamento">Departamento</label>
                                <div class="form-group">
                                    <select id="category_e" name="depa" class="form-control show-tick">
                                        @foreach($deps as $dep)
                                            <option value="{{$dep->id}}">{{$dep->dep_nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="pre_costo">Precio Costo</label>
                                <div class="input-group input-group-sm spinner" data-trigger="spinner">
                                    <span class="input-group-addon">$</span>
                                    <div class="form-line">
                                        <input type="text" class="form-control text-center" value="1" data-rule="currency" name="pre_costo" id="pre_costo">
                                    </div>
                                    <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="pre_vent">Precio Venta</label>
                                <div class="input-group input-group-sm spinner" data-trigger="spinner">
                                    <span class="input-group-addon">$</span>
                                    <div class="form-line">
                                        <input type="text" class="form-control text-center" value="1" data-rule="currency" name="pre_vent" id="pre_vent">
                                    </div>
                                    <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="pre_may">Precio Mayoreo</label>
                                <div class="input-group input-group-sm spinner" data-trigger="spinner">
                                    <span class="input-group-addon">$</span>
                                    <div class="form-line">
                                        <input type="text" class="form-control text-center" value="1" data-rule="currency" name="pre_may" id="pre_may">
                                    </div>
                                    <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="switch">
                                    <label>Este producto SI utiliza inventario <input type="checkbox" id="inv" name="inv" ><span class="lever switch-col-indigo"></span></label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="cat_inv">Cantidad Actual</label>
                                <div class="input-group input-group-sm spinner" data-trigger="spinner">
                                    <span class="input-group-addon">$</span>
                                    <div class="form-line">
                                        <input type="text" class="form-control text-center" value="1" data-rule="currency" name="cat_inv" id="cat_inv" >
                                    </div>
                                    <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="inv_min">Minimo</label>
                                <div class="input-group input-group-sm spinner" data-trigger="spinner">
                                    <span class="input-group-addon">$</span>
                                    <div class="form-line">
                                        <input type="text" class="form-control text-center" value="1" data-rule="currency" name="inv_min" id="inv_min" >
                                    </div>
                                    <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">GUARDAR</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="editProdModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-col-green">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Editar Usuario</h4>
                </div>
                <form id="edit_product" method="POST" action="{{ route('admin.prod_update') }}">
                    <div class="modal-body" style="background-color: #ffffff;color: #000 !important;">
                        @csrf
                        <div class="row">
                            <div class="col-md-3">
                                <label for="id">Codigo</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="edit_id" id="edit_id" class="form-control" placeholder="Ingresa el codigo del producto">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="edit_name">Nombre</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="edit_name" id="edit_name" class="form-control" placeholder="Nombre">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="edit_desc">Descripción</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input name="edit_desc"  id="edit_desc" class="form-control" placeholder="Desripción del producto" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-7">
                                <label for=""> Se Vende</label>
                                <div class="demo-radio-button">
                                    <input name="edit_umed" type="radio" id="edit_radio_7" class="radio-col-red" checked value="1"  required/>
                                    <label for="edit_radio_7">Por Unidad/Pza</label>
                                    <input name="edit_umed" type="radio" id="edit_radio_8" class="radio-col-pink" value="2"  />
                                    <label for="edit_radio_8">A Granel <span>(usa decimales)</span></label>
                                    <input name="edit_umed" type="radio" id="edit_radio_9" class="radio-col-purple" value="3" />
                                    <label for="edit_radio_9">Como Paquete <span>(kit)</span></label>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label for="edit_depa">Departamento</label>
                                <div class="form-group">
                                    <select id="edit_depa" name="edit_depa" class="form-control show-tick" required>
                                        @foreach($deps as $dep)
                                            <option value="{{$dep->id}}">{{$dep->dep_nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="edit_pre_costo">Precio Costo</label>
                                <div class="input-group input-group-sm spinner" data-trigger="spinner">
                                    <span class="input-group-addon">$</span>
                                    <div class="form-line">
                                        <input type="text" class="form-control text-center" value="1" data-rule="currency" name="edit_pre_costo" id="edit_pre_costo" required>
                                    </div>
                                    <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="edit_pre_vent">Precio Venta</label>
                                <div class="input-group input-group-sm spinner" data-trigger="spinner">
                                    <span class="input-group-addon">$</span>
                                    <div class="form-line">
                                        <input type="text" class="form-control text-center" value="1" data-rule="currency" name="edit_pre_vent" id="edit_pre_vent" required>
                                    </div>
                                    <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="edit_pre_may">Precio Mayoreo</label>
                                <div class="input-group input-group-sm spinner" data-trigger="spinner">
                                    <span class="input-group-addon">$</span>
                                    <div class="form-line">
                                        <input type="text" class="form-control text-center" value="1" data-rule="currency" name="edit_pre_may" id="edit_pre_may" required>
                                    </div>
                                    <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="switch">
                                    <label>Este producto SI utiliza inventario <input type="checkbox" id="edit_inv" name="edit_inv" ><span class="lever switch-col-indigo"></span></label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="edit_cat_inv">Cantidad Actual</label>
                                <div class="input-group input-group-sm spinner" data-trigger="spinner">
                                    <span class="input-group-addon">$</span>
                                    <div class="form-line">
                                        <input type="text" class="form-control text-center" value="1" data-rule="currency" name="edit_cat_inv" id="edit_cat_inv" >
                                    </div>
                                    <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="edit_inv_min">Minimo</label>
                                <div class="input-group input-group-sm spinner" data-trigger="spinner">
                                    <span class="input-group-addon">$</span>
                                    <div class="form-line">
                                        <input type="text" class="form-control text-center" value="1" data-rule="currency" name="edit_inv_min" id="edit_inv_min" >
                                    </div>
                                    <span class="input-group-addon">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">GUARDAR</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="deleteProdModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-col-red">
                <form name="delete_prod" id="delete_prod" method="POST" action="{{ route('admin.prod_delete') }}">
                    @csrf
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Eliminar Producto</h4>
                    </div>
                    <div class="modal-body" style="background-color: #ffffff;color: #000 !important;">
                        <p>¿Seguro que quieres eliminar este registro?</p>
                        <p class="text-warning">
                            <small>Esta acción no se puede deshacer.</small>
                        </p>
                        <input type="hidden" name="d_id" id="d_id" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">ELIMINAR</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <script src="plugins/jquery-spinner/js/jquery.spinner.js"></script>
    <script src="plugins/sweetalert/sweetalert-dev.js"></script>

    <!-- Custom Js -->
    <script src="js/pages/tables/jquery-datatable.js"></script>
    <script>
        $('#editProdModal').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var id = button.data('id');
            $('#edit_id').val(id);

            var nombre = button.data('nombre');
            $('#edit_name').val(nombre);

            var desc = button.data('desc');
            $('#edit_desc').val(desc);

            var medida = button.data('medida');
            var medidas = $('input:radio[name=edit_umed]');
            medidas.filter('[value='+medida+']').prop('checked', true);

            var depa = button.data('depa');
            var select = document.getElementById("edit_depa");
            for (var i = 1; i < select.length; i++) {
                if (select.options[i].text == depa) {
                    // seleccionamos el valor que coincide
                    select.selectedIndex = i;
                }
            }

            var cost = button.data('cost');
            $('#edit_pre_costo').val(cost);

            var vent = button.data('vent');
            $('#edit_pre_vent').val(vent);

            var premay = button.data('premay');
            $('#edit_pre_may').val(premay);

            var binv = button.data('binv');
            var exist = button.data('exist');
            var invmin = button.data('invmin');

            if(binv == 1) {
                $("#edit_inv").attr('checked', true);
                $('#edit_cat_inv').val(exist);
                $('#edit_inv_min').val(invmin);
            }
            else {
                $("#edit_inv :checkbox").attr('checked', false);
                $('#edit_cat_inv').val(0);

                $('#edit_inv_min').val(0);
            }

        });

        $('#deleteProdModal').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var id = button.data('id');
            $('#d_id').val(id);
        });

        $("#edit_product").submit(function(event) {
            var parametros = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('admin.prod_update') }}",
                data: parametros,
                beforeSend: function(objeto) {
                    $("#resultados").html("Enviando...");
                },
                success: function(datos) {
                    swal({
                        title: "Muy bien!",
                        text: datos.success,
                        icon: "success",
                        timer: 3000,
                        showConfirmButton: false,
                    });
                    setTimeout(function(){
                        window.location.reload();
                        $('#editProductModal').modal('hide');
                    },3000);
                }
            });
            event.preventDefault();
        });

        $("#add_product").submit(function(event) {
            var parametros = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('admin.prod_create') }}",
                data: parametros,
                beforeSend: function(objeto) {
                    $("#resultados").html("Enviando...");
                },
                success: function(datos) {
                    swal({
                        title: "Muy bien!",
                        text: datos.success,
                        icon: "success",
                        timer: 3000,
                        showConfirmButton: false,
                    });
                    setTimeout(function(){
                        window.location.reload();
                        $('#addProductModal').modal('hide');
                    },3000);
                },
                error: function(datos) {
                    console.log(datos.responseText);
                }
            });
            event.preventDefault();
        });

        $("#delete_prod").submit(function(event) {
            var parametros = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('admin.prod_delete') }}",
                data: parametros,
                beforeSend: function(objeto) {
                    $("#resultados").html("Enviando...");
                },
                success: function(datos) {
                    swal({
                        title: "Muy bien!",
                        text: datos.success,
                        icon: "success",
                        timer: 3000,
                        showConfirmButton: false,
                    });
                    setTimeout(function(){
                        window.location.reload();
                    },3000);
                }
            });
            event.preventDefault();
        });

        const checkbox = document.getElementById('inv')

        checkbox.addEventListener('change', (event) => {
            if (event.target.checked) {
                $('#cat_inv').attr('disable',false);
                $('#inv_min').attr('disable',false);
            } else {
                $('#cat_inv').attr('disable',true);
                $('#inv_min').attr('disable',true);
            }
        })
    </script>
@endsection

@extends('layouts.pv')

@section('title', 'APV | Usuarios')
@section('style')
    <link href="plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
    <link href="plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

@endsection

@section('menu')
<li>
    <a href="dashboard">
        <i class="material-icons">home</i>
        <span>Inicio</span>
    </a>
</li>
<li class="active">
    <a href="clientes">
        <i class="material-icons">assignment_ind</i>
        <span>Clientes</span>
    </a>
</li>
<li>
    <a href="productos">
        <i class="material-icons">shopping_cart</i>
        <span>Productos</span>
    </a>
</li>
<li>
    <a href="ventas">
        <i class="material-icons">attach_money</i>
        <span>Ventas</span>
    </a>
</li>
<li>
    <a href="departamentos">
        <i class="material-icons">list</i>
        <span>Departamentos</span>
    </a>
</li>
<li>
    <a href="rventas">
        <i class="material-icons">update</i>
        <span>Reporte de ventas</span>
    </a>
</li>
<li>
    <a href="inventario">
        <i class="material-icons">view_module</i>
        <span>Inventario</span>
    </a>
</li>
<li>
    <a href="usuarios">
        <i class="material-icons">face</i>
        <span>Cajeros</span>
    </a>
</li>
@endsection
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Administrar <b>Clientes</b>
                        <a href="#addClientModal" class="btn btn-success pull-right" data-toggle="modal">
                            <i class="material-icons">&#xE147;</i>
                            <span>Agregar nuevo cliente</span>
                        </a>
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Nombre</th>
                                <th class="text-center">Dirección</th>
                                <th class="text-center">Telefono</th>
                                <th class="text-center">Limite de Credito</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Nombre</th>
                                <th class="text-center">Dirección</th>
                                <th class="text-center">Telefono</th>
                                <th class="text-center">Limite de Credito</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                            </tfoot>
                            <tbody>
                                @php $i = 1; @endphp
                                @foreach($clients as $client)
                                    <tr class="">
                                        <td class="text-center">{{$i}}</td>
                                        <td class="text-center">{{$client->nombre}} {{$client->paterno}} {{$client->materno}}</td>
                                        <td class="text-center">{{$client->direccion}}</td>
                                        <td class="text-center">{{$client->telefono}}</td>
                                        <td class="text-center">
                                            @if($client->lim_credito==0)
                                                {{"Ilimitado"}}
                                            @else
                                                {{$client->lim_credito}}
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <a href="#" data-target="#editClientModal" class="edit" data-toggle="modal"
                                               data-paterno="{{$client->paterno}}"
                                               data-materno="{{$client->materno}}"
                                               data-id="{{$client->id}}"
                                               data-direccion="{{$client->direccion}}"
                                               data-telefono="{{$client->telefono}}"
                                               data-lim_credito="{{$client->lim_credito}}"
                                               data-nombre="{{$client->nombre}}"><i class="material-icons" data-toggle="tooltip"
                                                                               title="Editar"></i></a>
                                            <a href="#deleteClientModal" class="delete" data-toggle="modal"
                                               data-id="{{$client->id}}" data-name="{{$client->user_name}}"><i
                                                    class="material-icons" data-toggle="tooltip" title="Eliminar"></i></a>
                                        </td>
                                    </tr>
                                    @php $i ++; @endphp
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="addClientModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-col-blue">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Agregar Cliente</h4>
                </div>
                <form id="add_client" method="POST" action="{{ route('admin.client_create') }}">
                    <div class="modal-body" style="background-color: #ffffff;color: #000 !important;">
                        @csrf
                        <div class="row">
                                <div class="col-md-6">
                                    <label for="name">Nombre</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="name" id="name" class="form-control" placeholder="Ingresa el nombre del nuevo cliente">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="ape">Apellido Paterno</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="ape" id="ape" class="form-control" placeholder="Apellido paterno">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="ame">Apellido Materno</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="ame" id="ame" class="form-control" placeholder="Apellido materno">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="dir">Dirección</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="dir" id="dir" class="form-control" placeholder="Dirección">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="tel">Telefono</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" name="tel" id="tel" class="form-control" placeholder="Telefono" maxlength="10" minlength="10">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="limcred">Limite de Credito</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" name="limcred" id="limcred" class="form-control" placeholder="Limite de credito" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <p><span class="font-italic font-9s">Recuerde que si la cantidad del limite no es llenada o es cero el cliente no tendra limite de credito</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">GUARDAR</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="editClientModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-col-green">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Editar Cliente</h4>
                </div>
                <form id="edit_client" method="POST" action="{{ route('admin.client_update') }}">
                    <div class="modal-body" style="background-color: #ffffff;color: #000 !important;">
                        @csrf
                        <div class="row">
                            <input type="hidden" name="edit_id" id="edit_id"  value="">
                            <div class="col-md-6">
                                <label for="edit_name">Nombre</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="edit_name" id="edit_name" class="form-control" placeholder="Ingresa el nombre del cajero">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="edit_ape">Apellido Paterno</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="edit_ape" id="edit_ape" class="form-control" placeholder="Apellido paterno">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="edit_ame">Apellido Materno</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="edit_ame" id="edit_ame" class="form-control" placeholder="Apellido materno">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="edit_dir">Dirección</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="edit_dir" id="edit_dir" class="form-control" placeholder="Dirección">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="edit_tel">Telefono</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" name="edit_tel" id="edit_tel" class="form-control" placeholder="Telefono" maxlength="10" minlength="10">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="edit_limcred">Limite de Credito</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" name="edit_limcred" id="edit_limcred" class="form-control" placeholder="Limite de credito">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">GUARDAR</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="deleteClientModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-col-red">
                <form id="delete_client" method="POST" action="{{ route('admin.client_delete') }}">
                    @csrf
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Eliminar Usuario</h4>
                    </div>
                        <div class="modal-body" style="background-color: #ffffff;color: #000 !important;">
                            <p>¿Seguro que quieres eliminar este registro?</p>
                            <p class="text-warning">
                                <small>Esta acción no se puede deshacer.</small>
                            </p>
                            <input type="hidden" name="d_id" id="d_id" value="">
                        </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">ELIMINAR</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <script src="plugins/sweetalert/sweetalert-dev.js"></script>
    <!-- Custom Js -->
    <script src="js/pages/tables/jquery-datatable.js"></script>
    <script>

        $('#editClientModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var id = button.data('id');
            $('#edit_id').val(id);
            var nombre = button.data('nombre');
            $('#edit_name').val(nombre);
            var paterno = button.data('paterno');
            $('#edit_ape').val(paterno);
            var materno = button.data('materno');
            $('#edit_ame').val(materno);
            var dir = button.data('direccion');
            $('#edit_dir').val(dir);
            var tel = button.data('telefono');
            $('#edit_tel').val(tel);
            var limcred = button.data('lim_credito');
            $('#edit_limcred').val(limcred);
        });

        $('#deleteClientModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var id = button.data('id');
            $('#d_id').val(id);
        });

        $("#edit_client").submit(function (event) {
            var parametros = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('admin.client_update') }}",
                data: parametros,
                beforeSend: function (objeto) {
                    $("#resultados").html("Enviando...");
                },
                success: function (datos) {
                    swal({
                        title: "Muy bien!",
                        text: datos.success,
                        icon: "success",
                        timer: 3000,
                        showConfirmButton: false,
                    });
                    setTimeout(function(){
                        window.location.reload();
                        $('#editClientModal').modal('hide');
                    },3000);
                }
            });
            event.preventDefault();
        });

        $("#add_client").submit(function (event) {
            var parametros = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('admin.client_create') }}",
                data: parametros,
                beforeSend: function (objeto) {
                    $("#resultados").html("Enviando...");
                },
                success: function (datos) {
                    swal({
                        title: "Muy bien!",
                        text: "Se guardo el registro!",
                        icon: "success",
                        timer: 3000,
                        showConfirmButton: false,
                    });
                    setTimeout(function(){
                        window.location.reload();
                        $('#addClientModal').modal('hide');
                    },3000);
                },
                error: function (datos) {
                    console.log(datos.responseText);
                }
            });
            event.preventDefault();
        });

        $("#delete_client").submit(function (event) {
            var parametros = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('admin.client_delete') }}",
                data: parametros,
                beforeSend: function (objeto) {
                    $("#resultados").html("Enviando...");
                },
                success: function (datos) {
                    swal({
                        title: "Muy bien!",
                        text: datos.success,
                        icon: "success",
                        timer: 5000,
                        showConfirmButton: false,
                    });
                    setTimeout(function(){
                        window.location.reload();
                        self.close();
                    },3000);
                }
            });
            event.preventDefault();
        });


    </script>
@endsection

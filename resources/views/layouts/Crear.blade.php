<?php
/**
 * Created by PhpStorm.
 * User: Rocha
 * Date: 21/06/2019
 * Time: 14:55
 */
?>
<a href="javascript:void(0)" data-toggle="tooltip"  data-id="{{ $id }}" data-original-title="Edit" class="edit btn btn-success edit-user">
    Edit
</a>
<a href="javascript:void(0);" id="delete-user" data-toggle="tooltip" data-original-title="Delete" data-id="{{ $id }}" class="delete btn btn-danger">
    Delete
</a>

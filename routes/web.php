<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(["register" => false]);

Route::get('/home', 'PvController@index2')->name('home');

Route::get('/dashboard', 'PvController@dashboard')->name('admin.dash');

Route::get('/usuarios', 'UsuariosController@user')->name('admin.user');
Route::post('/usuarios_create', 'UsuariosController@user_create')->name('admin.user_create');
Route::post('/usuarios_delete', 'UsuariosController@user_delete')->name('admin.user_delete');
Route::post('/usuarios_update', 'UsuariosController@user_update')->name('admin.user_update');

Route::get('/clientes', 'ClientesController@client')->name('admin.client');
Route::post('/clientes_create', 'ClientesController@client_create')->name('admin.client_create');
Route::post('/clientes_delete', 'ClientesController@client_delete')->name('admin.client_delete');
Route::post('/clientes_update', 'ClientesController@client_update')->name('admin.client_update');

Route::get('/departamentos', 'DepartamentosController@deps')->name('admin.deps');
Route::post('/departamentos_create', 'DepartamentosController@deps_create')->name('admin.dep_create');
Route::post('/departamentos_delete', 'DepartamentosController@deps_delete')->name('admin.dep_delete');
Route::post('/departamentos_update', 'DepartamentosController@deps_update')->name('admin.dep_update');

Route::get('/productos', 'ProductosController@prods')->name('admin.prods');
Route::post('/prod_create', 'ProductosController@prods_create')->name('admin.prod_create');
Route::post('/prod_delete', 'ProductosController@prods_delete')->name('admin.prod_delete');
Route::post('/prod_update', 'ProductosController@prods_update')->name('admin.prod_update');

Route::get('/inventario', 'InventariosController@inven')->name('admin.inv');

Route::get('/ventas', 'VentasController@iindex')->name('admin.inv');
